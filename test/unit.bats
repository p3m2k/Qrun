load lib/assertions.bash
load lib/utils.bash
load lib/mocker.bash
load lib/common.bash
load meta.bash



# Test Support
#

setup_file() {
    # NB: Variables in setup_file() and teardown_file() must be exported!
    # Global vars declared in sourced SWUT can be overridden in test cases with
    # `local VARNAME=VAL` declarations.

    assert_dependencies $dependencies
}


setup() {
    mock="mock -d $BATS_TEST_TMPDIR"
}


fake_cmd() {
    local -r first="$1"
    local -r second="$2"

    echo "First: \"$first\""
    echo "Second: \"$second\""

    if [[ "$first" == "one" ]]  &&  [[ "$second" == "one" ]] ; then
        echo "Snake eyes!  Failure so sad."
        return 1
    else
        echo "Success!"
        return 0
    fi
} ; export -f fake_cmd



# Common Tests
#

@test "echo_verbose()" {
    local output="This is only a test."

    source_swut "$swut"
    local -r verbose=1
    run echo_verbose "$output"

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "$output" ]]  # FIXME: confirm stderr
}


@test "echo_func_err()" {
    local err_msg="This is only a test."

    source_swut "$swut"
    run echo_func_err "$err_msg"

    assert_status $expected_status_success
    assert_lines_qty 1
    # NB: The calling function name depends on bats internals!
    [[ "${lines[0]}" == "bats_merge_stdout_and_stderr: $err_msg" ]]  # FIXME: confirm stderr
}


@test "maybe(), success" {
    source_swut "$swut"
    run maybe fake_cmd "one" "two"

    assert_status $expected_status_success
    assert_lines_qty 3
    [[ "${lines[0]}" == "First: \"one\"" ]]
    [[ "${lines[1]}" == "Second: \"two\"" ]]
    [[ "${lines[2]}" == "Success!" ]]
}


@test "maybe(), failure" {
    source_swut "$swut"
    run maybe fake_cmd "one" "one"

    assert_status $expected_status_error
    assert_lines_qty 3
    [[ "${lines[0]}" == "First: \"one\"" ]]
    [[ "${lines[1]}" == "Second: \"one\"" ]]
    [[ "${lines[2]}" == "Snake eyes!  Failure so sad." ]]
}


@test "maybe(), dry run mode" {
    local dry_run=1  # Necessary to override global dry_run in SWUT.

    source_swut "$swut"
    run maybe fake_cmd "seven" "three"

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "fake_cmd seven three" ]]
}



# Script-specific
#

@test "get_checksum(), success" {
    local d='This is not a drill!'

    source_swut "$swut"
    run get_checksum "$d"

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "259e1d6def2dc5a1a892e1923c610889e0c64495e817656fdfee1ecdb4f7bcbf" ]]
}


@test "get_checksum(), empty arg" {
    source_swut "$swut"
    run get_checksum ""

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b" ]]
}


@test "get_checksum(), missing arg" {
    source_swut "$swut"
    run get_checksum

    assert_status $expected_status_error
    assert_lines_qty 1
    [[ "${lines[0]}" == "get_checksum: Missing argument!" ]]
}


@test "get_re_enqueue_count(), success" {
    local metadata_path="test/assets/qrun.dat"
    local ds_path_checksum="68972cf41dce420c57592d07a8ec5a773c78826e5d8630f04210982840e5dbf2"
    local d='This is not a drill!'

    source_swut "$swut"
    run get_re_enqueue_count "$d"

    assert_lines_qty 1
    [[ "${lines[0]}" == "42" ]]
}


@test "get_re_enqueue_count(), unknown data structure" {
    local metadata_path="test/assets/qrun.dat"
    local ds_path_checksum="68972cf41dce420c57592d07a8ec5a773c78826e5d8630f04210982840e5dbf2"
    local d='This is only a test.'

    source_swut "$swut"
    run get_re_enqueue_count "$d"

    assert_lines_qty 1
    [[ "${lines[0]}" == "0" ]]
}


@test "get_re_enqueue_count(), unknown data" {
    local metadata_path="test/assets/qrun.dat"
    local ds_path_checksum="68972cf41dce420c57592d07a8ec5a773c78826e5d8630f04210982840e5dbf2"
    local d='This is only a test.'

    source_swut "$swut"
    run get_re_enqueue_count "$d"

    assert_lines_qty 1
    [[ "${lines[0]}" == "0" ]]
}


@test "set_re_enqueue_count(), existing entry" {
    d='This is not a drill!'
    new_count=57

    local ds_path_checksum="68972cf41dce420c57592d07a8ec5a773c78826e5d8630f04210982840e5dbf2"
    local metadata_path="${BATS_TEST_TMPDIR}/qrun.dat"
    cp test/assets/qrun.dat "$metadata_path"

    source_swut "$swut"
    run set_re_enqueue_count "$d" "$new_count"

    assert_lines_qty 0
    assert_file_grep_string "$metadata_path" "$ds_path_checksum | 259e1d6def2dc5a1a892e1923c610889e0c64495e817656fdfee1ecdb4f7bcbf | $new_count"
}


@test "set_re_enqueue_count(), new entry" {
    d='This is only a test.'
    new_count=57

    local ds_path_checksum="68972cf41dce420c57592d07a8ec5a773c78826e5d8630f04210982840e5dbf2"
    local metadata_path="${BATS_TEST_TMPDIR}/qrun.dat"
    cp test/assets/qrun.dat "$metadata_path"

    source_swut "$swut"
    run set_re_enqueue_count "$d" "$new_count"

    assert_lines_qty 0
    assert_file_grep_string "$metadata_path" "$ds_path_checksum | d7fd69a4115428c9d669ca05c44e54687253d2a3f5f7424e4ce0881a9b7b48ac | $new_count"
}


@test "increment_re_enqueue_count(), success" {
    local d='This is not a drill!'
    local count_current=42
    local count_new=43

    source_swut "$swut"
    $mock get_re_enqueue_count $count_current
    $mock set_re_enqueue_count
    run increment_re_enqueue_count "$d"

    assert_lines_qty 0
    assert_file_cmp_string "${MOCK_GET_RE_ENQUEUE_COUNT_CALLS_DIR}/1.txt" "$d"
    assert_file_cmp_string "${MOCK_SET_RE_ENQUEUE_COUNT_CALLS_DIR}/1.txt" "$d $count_new"
}


@test "run_command_on_data(), success" {
    local verbose=1
    local cmd_line="echo '> %s <'"
    local d='This is only:
a test.'

    source_swut "$swut"
    $mock maybe
    # Can't mock `printf`, because it breaks BATS.
    run run_command_on_data "$d"

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "Command succeeded." ]]
    assert_file_cmp_string "${MOCK_MAYBE_CALLS_DIR}/1.txt" "echo '> $d <'"
}


@test "run_command_on_data(), excess format specs, success" {
    local verbose=1
    local cmd_line="echo '%s' '%s'"
    local d='This is only a test.'

    source_swut "$swut"
    $mock maybe
    # Can't mock `printf`, because it breaks BATS.
    run run_command_on_data "$d"

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "Command succeeded." ]]
    assert_file_cmp_string "${MOCK_MAYBE_CALLS_DIR}/1.txt" "echo '$d' ''"
}


@test "run_command_on_data(), insuff. format specs, success" {
    # More like *no* format specs, amirite?  Not exactly an expected use case,
    # but what the hay...
    local verbose=1
    local cmd_line="echo ''"
    local d='This is only a test.'

    source_swut "$swut"
    $mock maybe
    # Can't mock `printf`, because it breaks BATS.
    run run_command_on_data "$d"

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "Command succeeded." ]]
    assert_file_cmp_string "${MOCK_MAYBE_CALLS_DIR}/1.txt" "echo ''"
}


@test "run_command_on_data(), unquoted printf args opt, success" {
    local verbose=1
    local quote_printf_args=0
    local cmd_line="echo '%s' '%s' '%s' '%s' '%s'"
    local d="This is only a test."

    source_swut "$swut"
    $mock maybe
    # Can't mock `printf`, because it breaks BATS.
    run run_command_on_data "$d"

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "Command succeeded." ]]
    assert_file_cmp_string "${MOCK_MAYBE_CALLS_DIR}/1.txt" "echo 'This' 'is' 'only' 'a' 'test.'"
}


@test "run_command_on_data(), unquoted printf args opt, excess format specs, success" {
    local verbose=1
    local quote_printf_args=0
    local cmd_line="echo '%s' '%s' '%s' '%s' '%s' '%s'"
    local d="This is only a test."

    source_swut "$swut"
    $mock maybe
    # Can't mock `printf`, because it breaks BATS.
    run run_command_on_data "$d"

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "Command succeeded." ]]
    assert_file_cmp_string "${MOCK_MAYBE_CALLS_DIR}/1.txt" "echo 'This' 'is' 'only' 'a' 'test.' ''"
}


@test "run_command_on_data(), unquoted printf args opt, insuff. format specs, success" {
    local verbose=1
    local quote_printf_args=0
    local cmd_line="echo '%s' '%s' '%s'"
    local d="This is only a test."

    source_swut "$swut"
    $mock maybe
    # Can't mock `printf`, because it breaks BATS.
    run run_command_on_data "$d"

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "Command succeeded." ]]
    assert_file_cmp_string "${MOCK_MAYBE_CALLS_DIR}/1.txt" "echo 'This' 'is' 'only'echo 'a' 'test.' ''"
}


@test "run_command_on_data(), dry run mode, success" {
    local verbose=1
    local dry_run=1
    local cmd_line="echo '%s'"
    local d='This is only a test.'

    source_swut "$swut"
    $mock maybe
    run run_command_on_data "$d"

    assert_status $expected_status_success
    assert_lines_qty 0
    assert_file_cmp_string "${MOCK_MAYBE_CALLS_DIR}/1.txt" "echo '$d'"
}


@test "run_command_on_data(), fail" {
    local verbose=1
    local cmd_line="echo '%s'"
    local d='This is not a drill!'

    source_swut "$swut"
    $mock -r 1 maybe
    run run_command_on_data "$d"

    assert_status $expected_status_error
    assert_lines_qty 1
    [[ "${lines[0]}" == "Command failed." ]]
    assert_file_cmp_string "${MOCK_MAYBE_CALLS_DIR}/1.txt" "echo '$d'"
}


@test "re_enqueue_data_maybe(), re-enqueue limit default" {
    local verbose=1
    local d='Does not matter.'

    source_swut "$swut"
    run re_enqueue_data_maybe "$d"

    assert_lines_qty 0
}


@test "re_enqueue_data_maybe(), re-enqueue mode, under limit" {
    local verbose=1
    local re_enqueue_limit=10
    local d='This is not a drill!'
    local ds_path="/does/not/matter"

    source_swut "$swut"
    $mock get_re_enqueue_count 3
    $mock dirtyq
    $mock increment_re_enqueue_count
    run re_enqueue_data_maybe "$d"

    assert_lines_qty 1
    [[ "${lines[0]}" == "Re-enqueuing data." ]]
    assert_file_cmp_string "${MOCK_GET_RE_ENQUEUE_COUNT_CALLS_DIR}/1.txt" "$d"
    assert_file_cmp_string "${MOCK_DIRTYQ_CALLS_DIR}/1.txt" "enqueue $ds_path $d"
    assert_file_cmp_string "${MOCK_INCREMENT_RE_ENQUEUE_COUNT_CALLS_DIR}/1.txt" "$d"
}


@test "re_enqueue_data_maybe(), re-enqueue mode, over limit" {
    local verbose=1
    local re_enqueue_limit=1
    local d='This is not a drill!'
    local ds_path="/does/not/matter"

    source_swut "$swut"
    $mock get_re_enqueue_count 3
    run re_enqueue_data_maybe "$d"

    assert_lines_qty 1
    [[ "${lines[0]}" == "Re-enqueue limit reached.  Not re-enqueuing data." ]]
    assert_file_cmp_string "${MOCK_GET_RE_ENQUEUE_COUNT_CALLS_DIR}/1.txt" "$d"
}


@test "process_queue()" {
    local verbose=1
    local ds_path="/does/not/matter"

    source_swut "$swut"
    $mock dirtyq "Fee"
    $mock dirtyq "Fi"
    $mock dirtyq "Fo"
    $mock dirtyq "Fum"
    $mock -r 1 dirtyq
    $mock -r 1 run_command_on_data
    $mock run_command_on_data
    $mock -r 1 run_command_on_data
    $mock run_command_on_data
    $mock re_enqueue_data_maybe
    run process_queue

    assert_lines_qty 4
    [[ "${lines[0]}" == "Data: 'Fee'" ]]
    [[ "${lines[1]}" == "Data: 'Fi'" ]]
    [[ "${lines[2]}" == "Data: 'Fo'" ]]
    [[ "${lines[3]}" == "Data: 'Fum'" ]]

    assert_file_cmp_string "${MOCK_DIRTYQ_CALLS_DIR}/1.txt" "dequeue $ds_path"
    assert_file_cmp_string "${MOCK_DIRTYQ_CALLS_DIR}/2.txt" "dequeue $ds_path"
    assert_file_cmp_string "${MOCK_DIRTYQ_CALLS_DIR}/3.txt" "dequeue $ds_path"
    assert_file_cmp_string "${MOCK_DIRTYQ_CALLS_DIR}/4.txt" "dequeue $ds_path"
    assert_file_cmp_string "${MOCK_RUN_COMMAND_ON_DATA_CALLS_DIR}/1.txt" "Fee"
    assert_file_cmp_string "${MOCK_RUN_COMMAND_ON_DATA_CALLS_DIR}/2.txt" "Fi"
    assert_file_cmp_string "${MOCK_RUN_COMMAND_ON_DATA_CALLS_DIR}/3.txt" "Fo"
    assert_file_cmp_string "${MOCK_RUN_COMMAND_ON_DATA_CALLS_DIR}/4.txt" "Fum"
    assert_file_cmp_string "${MOCK_RE_ENQUEUE_DATA_MAYBE_CALLS_DIR}/1.txt" "Fee"
    assert_file_cmp_string "${MOCK_RE_ENQUEUE_DATA_MAYBE_CALLS_DIR}/2.txt" "Fo"
}





# vim: syntax=bash tw=0
