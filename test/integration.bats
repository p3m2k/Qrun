load lib/assertions.bash
load lib/utils.bash
load lib/mocker.bash
load lib/common.bash
load meta.bash


# Test support
#

setup_file() {
    # NB: Variables in setup_file() and teardown_file() must be exported!

    assert_dependencies $dependencies
}


fail_on_two() {
    if [[ "$1" == "two" ]] ; then
        echo "$1 FAILS!!"
        return 1
    else
        echo "$1 succeeds."
        return 0
    fi
} ; export -f fail_on_two


make_test_queue() {
    local queue_d="$1"

    dirtyq -d init "$queue_d"
    dirtyq enq "$queue_d" "one"
    dirtyq enq "$queue_d" "two"
    dirtyq enq "$queue_d" "$(echo "eenie" ; echo "meenie")"
}



# Common options
#

@test "no opts, no args" {
    run exec_swut

    assert_status $expected_status_error
    assert_lines_qty 1
    [[ "${lines[0]}" == "$expected_usage_short" ]]
}


@test "missing opt arg" {
    run exec_swut -f

    assert_status $expected_status_error
    assert_lines_qty 1
    [[ "${lines[0]}" == "$expected_usage_short" ]]
}


@test "invalid opt" {
    run exec_swut -z

    assert_status $expected_status_error
    assert_lines_qty 2
    [[ "${lines[0]}" == "Invalid option: z" ]]
    [[ "${lines[1]}" == "$expected_usage_short" ]]
}


@test "help" {
    run exec_swut -h

    assert_status $expected_status_success
    assert_lines_qty 11  # bats ignores blank lines.
    [[ "$output" == "$expected_usage_long" ]]
}


@test "version" {
    run exec_swut -v

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "$swut $expected_version" ]]
}


@test "verbose option" {
    run exec_swut -V

    assert_status $expected_status_error
    assert_lines_qty 2
    [[ "${lines[0]}" == "Verbose mode requested." ]]
    [[ "${lines[1]}" == "$expected_usage_short" ]]
}


@test "dry run option" {
    run exec_swut -n

    assert_status $expected_status_error
    assert_lines_qty 2
    [[ "${lines[0]}" == "Dry run mode requested." ]]
    [[ "${lines[1]}" == "$expected_usage_short" ]]
}



# Script-specific
#

@test "queue run, fail limit option, invalid value" {
    run exec_swut -f 0 /does/not/matter immaterial

    assert_status $expected_status_error
    assert_lines_qty 2
    [[ "${lines[0]}" == "Limit must be one or greater!" ]]
    [[ "${lines[1]}" == "$expected_usage_short" ]]
}

@test "queue run, re-enqueue limit option, invalid value" {
    run exec_swut -r -1 /does/not/matter immaterial

    assert_status $expected_status_error
    assert_lines_qty 2
    [[ "${lines[0]}" == "Limit must be zero or greater!" ]]
    [[ "${lines[1]}" == "$expected_usage_short" ]]
}


@test "queue run, no options, no failers" {
    test_q="${BATS_TEST_TMPDIR}/test_q"
    make_test_queue "$test_q"

    run exec_swut "$test_q" echo \"%s\"

    assert_status $expected_status_success
    assert_lines_qty 4
    [[ "${lines[0]}" == "one" ]]
    [[ "${lines[1]}" == "two" ]]
    [[ "${lines[2]}" == "eenie" ]]
    [[ "${lines[3]}" == "meenie" ]]
}


@test "queue run, unquoted printf args opt, no failers" {
    # This has the same result as insufficient printf format specs.

    test_q="${BATS_TEST_TMPDIR}/test_q"
    make_test_queue "$test_q"

    run exec_swut -q "$test_q" echo \"%s\"

    assert_status $expected_status_success
    assert_lines_qty 4
    [[ "${lines[0]}" == "printf will be called with unquoted arguments." ]]
    [[ "${lines[1]}" == "one" ]]
    [[ "${lines[2]}" == "two" ]]
    [[ "${lines[3]}" == "eenieecho meenie" ]]
}


@test "queue run, dry run mode" {
    test_q="${BATS_TEST_TMPDIR}/test_q"
    make_test_queue "$test_q"

    run exec_swut -n "$test_q" fail_on_two \"%s\"

    assert_status $expected_status_success
    assert_lines_qty 5
    [[ "${lines[0]}" == "Dry run mode requested." ]]
    [[ "${lines[1]}" == "fail_on_two \"one\"" ]]
    [[ "${lines[2]}" == "fail_on_two \"two\"" ]]
    [[ "${lines[3]}" == "fail_on_two \"eenie" ]]
    [[ "${lines[4]}" == "meenie\"" ]]
}


@test "queue run, no options, one failer" {
    test_q="${BATS_TEST_TMPDIR}/test_q"
    make_test_queue "$test_q"

    run exec_swut "$test_q" fail_on_two \"%s\"

    assert_status $expected_status_success
    assert_lines_qty 4
    [[ "${lines[0]}" == "one succeeds." ]]
    [[ "${lines[1]}" == "two FAILS!!" ]]
    [[ "${lines[2]}" == "eenie" ]]
    [[ "${lines[3]}" == "meenie succeeds." ]]
}


@test "queue run, one failer, fail limit 5" {
    test_q="${BATS_TEST_TMPDIR}/test_q"
    make_test_queue "$test_q"

    run exec_swut -f 5 "$test_q" fail_on_two \"%s\"

    assert_status $expected_status_success
    assert_lines_qty 8
    [[ "${lines[0]}" == "one succeeds." ]]
    [[ "${lines[1]}" == "two FAILS!!" ]]
    [[ "${lines[2]}" == "${lines[2]}" ]]
    [[ "${lines[3]}" == "${lines[2]}" ]]
    [[ "${lines[4]}" == "${lines[2]}" ]]
    [[ "${lines[5]}" == "${lines[2]}" ]]
    [[ "${lines[6]}" == "eenie" ]]
    [[ "${lines[7]}" == "meenie succeeds." ]]
}


@test "queue run, verbose mode, re-enqueue limit 1" {
    test_q="${BATS_TEST_TMPDIR}/test_q"
    make_test_queue "$test_q"
    export XDG_DATA_HOME="${BATS_TEST_TMPDIR}"  # Don't muck w/ user's $metadata_path

    mock="mock -d $BATS_TEST_TMPDIR"
    $mock faker "one good"
    $mock -r 1 faker "two bad"
    $mock faker "eenie meenie good"
    $mock -r 1 faker "two bad"

    run exec_swut -V -r 1 "$test_q" faker -foo \"%s\"

    assert_status $expected_status_success
    assert_lines_qty 21
    [[ "${lines[0]}" == "Verbose mode requested." ]]

    [[ "${lines[1]}" == "Data: 'one'" ]]
    [[ "${lines[2]}" == "Executing command: faker -foo \"one\"" ]]
    [[ "${lines[3]}" == "one good" ]]
    [[ "${lines[4]}" == "Command succeeded." ]]

    [[ "${lines[5]}" == "Data: 'two'" ]]
    [[ "${lines[6]}" == "Executing command: faker -foo \"two\"" ]]
    [[ "${lines[7]}" == "two bad" ]]
    [[ "${lines[8]}" == "Command failed." ]]
    [[ "${lines[9]}" == "Re-enqueuing data." ]]

    [[ "${lines[10]}" == "Data: 'eenie" ]]
    [[ "${lines[11]}" == "meenie'" ]]
    [[ "${lines[12]}" == "Executing command: faker -foo \"eenie" ]]
    [[ "${lines[13]}" == "meenie\"" ]]
    [[ "${lines[14]}" == "eenie meenie good" ]]
    [[ "${lines[15]}" == "Command succeeded." ]]

    [[ "${lines[16]}" == "Data: 'two'" ]]
    [[ "${lines[17]}" == "Executing command: faker -foo \"two\"" ]]
    [[ "${lines[18]}" == "two bad" ]]
    [[ "${lines[19]}" == "Command failed." ]]
    [[ "${lines[20]}" == "Re-enqueue limit reached.  Not re-enqueuing data." ]]

    assert_file_cmp_string "${MOCK_FAKER_CALLS_DIR}/1.txt" "-foo one"
    assert_file_cmp_string "${MOCK_FAKER_CALLS_DIR}/2.txt" "-foo two"
    assert_file_cmp_string "${MOCK_FAKER_CALLS_DIR}/3.txt" "-foo eenie
meenie"
    assert_file_cmp_string "${MOCK_FAKER_CALLS_DIR}/4.txt" "-foo two"
}


@test "queue run, verbose mode, fail limit 2, re-enqueue limit 2" {
    test_q="${BATS_TEST_TMPDIR}/test_q"
    make_test_queue "$test_q"
    export XDG_DATA_HOME="${BATS_TEST_TMPDIR}"  # Don't muck w/ user's $metadata_path

    mock="mock -d $BATS_TEST_TMPDIR"
    $mock faker "one good"
    $mock -r 1 faker "two bad"
    $mock -r 1 faker "two bad"
    $mock faker "eenie meenie good"
    $mock -r 1 faker "two bad"
    $mock -r 1 faker "two bad"
    $mock -r 1 faker "two bad"
    $mock -r 1 faker "two bad"

    run exec_swut -V -f 2 -r 2 "$test_q" faker -bar \"%s\"

    assert_status $expected_status_success
    assert_lines_qty 32
    [[ "${lines[0]}" == "Verbose mode requested." ]]

    [[ "${lines[1]}" == "Data: 'one'" ]]
    [[ "${lines[2]}" == "Executing command: faker -bar \"one\"" ]]
    [[ "${lines[3]}" == "one good" ]]
    [[ "${lines[4]}" == "Command succeeded." ]]

    [[ "${lines[5]}" == "Data: 'two'" ]]
    [[ "${lines[6]}" == "Executing command: faker -bar \"two\"" ]]
    [[ "${lines[7]}" == "two bad" ]]

    [[ "${lines[8]}" == "Executing command: faker -bar \"two\"" ]]
    [[ "${lines[9]}" == "two bad" ]]
    [[ "${lines[10]}" == "Command failed." ]]
    [[ "${lines[11]}" == "Re-enqueuing data." ]]

    [[ "${lines[12]}" == "Data: 'eenie" ]]
    [[ "${lines[13]}" == "meenie'" ]]
    [[ "${lines[14]}" == "Executing command: faker -bar \"eenie" ]]
    [[ "${lines[15]}" == "meenie\"" ]]
    [[ "${lines[16]}" == "eenie meenie good" ]]
    [[ "${lines[17]}" == "Command succeeded." ]]

    [[ "${lines[18]}" == "Data: 'two'" ]]
    [[ "${lines[19]}" == "Executing command: faker -bar \"two\"" ]]
    [[ "${lines[20]}" == "two bad" ]]

    [[ "${lines[21]}" == "Executing command: faker -bar \"two\"" ]]
    [[ "${lines[22]}" == "two bad" ]]
    [[ "${lines[23]}" == "Command failed." ]]
    [[ "${lines[24]}" == "Re-enqueuing data." ]]

    [[ "${lines[25]}" == "Data: 'two'" ]]
    [[ "${lines[26]}" == "Executing command: faker -bar \"two\"" ]]
    [[ "${lines[27]}" == "two bad" ]]

    [[ "${lines[28]}" == "Executing command: faker -bar \"two\"" ]]
    [[ "${lines[29]}" == "two bad" ]]
    [[ "${lines[30]}" == "Command failed." ]]
    [[ "${lines[31]}" == "Re-enqueue limit reached.  Not re-enqueuing data." ]]

    assert_file_cmp_string "${MOCK_FAKER_CALLS_DIR}/1.txt" "-bar one"
    assert_file_cmp_string "${MOCK_FAKER_CALLS_DIR}/2.txt" "-bar two"
    assert_file_cmp_string "${MOCK_FAKER_CALLS_DIR}/3.txt" "-bar two"
    assert_file_cmp_string "${MOCK_FAKER_CALLS_DIR}/4.txt" "-bar eenie
meenie"
    assert_file_cmp_string "${MOCK_FAKER_CALLS_DIR}/5.txt" "-bar two"
    assert_file_cmp_string "${MOCK_FAKER_CALLS_DIR}/6.txt" "-bar two"
    assert_file_cmp_string "${MOCK_FAKER_CALLS_DIR}/7.txt" "-bar two"
    assert_file_cmp_string "${MOCK_FAKER_CALLS_DIR}/8.txt" "-bar two"
}





# vim: syntax=bash tw=0
