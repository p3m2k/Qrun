swut="qrun"
dependencies="dirtyq"

expected_version="1.2.0"

expected_status_success=0
expected_status_error=1

expected_usage_short="$swut [-h] [-v] [-V] [-n] [-f N] [-r N] [-t] [-b] [-q] QUEUE_PATH COMMAND"
expected_usage_long="$expected_usage_short
OPTIONS
    -h    Print usage
    -v    Print version
    -V    Verbose mode
    -n    Dry run mode
    -f    Limit to N consecutive failures [default 1]
    -r    Re-enqueue limit [default 0]
    -t    Do not re-enqueue data when terminated
    -b    Do not use Bash printf builtin
    -q    Do not quote printf data arguments"

expected_output_dry_run_opt="Dry run mode requested."
