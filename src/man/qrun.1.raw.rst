#######
${SLUG}
#######

${DESCRIPTION}
==============

:author: ${AUTHOR_NAME}
:contact: ${AUTHOR_CONTACT}
:copyright: © ${COPYRIGHT_YEARS} ${COPYRIGHT_OWNER} <${COPYRIGHT_CONTACT}>
:manual_section: 1
:version: ${RELEASE_VERSION}
:date: ${RELEASE_DATE}




Synopsis
--------

${SLUG} [-h] [-v] [-V] [-n] [-f N] [-r N] [-t] [-b] [-q] QUEUE_PATH COMMAND"



Description
-----------

``${SLUG}`` takes a path to a dirtyQ data structure and a
``printf``-formatted command line, and runs the command line on every
item in the data structure, with the data passed as the final argument.
E.g., given a queue with data items "one", "two" and "three":

.. code:: shell

   $ ${SLUG} /PATH/TO/DATA echo %s
   one
   two
   three



Options
-------

-h
    Print usage.
-v
    Print version.
-V
    Verbose mode
-n
    Dry run mode
-f
    Limit to N consecutive failures [default 1]
-r
    Re-enqueue limit [default 0]
-t
    Do not re-enqueue data when terminated
-b
    Do not use Bash printf builtin
-q
    Do not quote printf data arguments



Files
-----

``$XDG_DATA_HOME/${SLUG}.dat``
    Metadata used to keep track of the number of times the data objects
    of a ``dirtyq`` data structure are re-enqueued by ``${SLUG}``.
    Implemented as a table of pipe-delimited records, each record
    consisting of three fields: the path to the data structure, the data
    being re-enqueued, and the re-enqueue count.  Data structure path
    and data are both hashed via ``sha256sum``, which keeps the table
    manageable and the data secure.



Exit Status
-----------

0
    Success
1
    Error



Notes
-----

- ``${SLUG}`` will run the specified command line against the current
  data until the command passes (exit value of zero) or until the fail
  limit is (``-f``) reached.

- If a command fails to pass (i.e., the fail limit is reached), and the
  re-enqueue limit (``-r``) is set to a non-zero value, ``${SLUG}`` will
  re-enqueue the current data and return (eventually) to give it another
  try.

- The ``-t`` option ("Do not re-enqueue data when terminated") prevents
  ``${SLUG}`` from re-enqueuing the current data when it receives a TERM
  signal (e.g., the user pressed control-C).  The default behavior is to
  re-enqueue the current data, the assumption being that the data should
  not be discarded unless the command run against it has passed.

- In dry run mode, ``${SLUG}`` will still make real calls to ``dirtyq``,
  until the data structure is empty; the specified command line will be
  echoed to stdout.  Make a backup of the data structure before
  experimenting with dry run mode.

- Despite the name, ``${SLUG}`` can be used on a dirtyQ stack as well as
  a queue.  The results will be the same, however, since a *pop* is
  identical to a *dequeue*.  The re-enqueue option doesn't make much
  sense with a stack.

- Mind your command format string conversion specifiers!  Absent the
  ``-q`` option (unquoted ``printf`` data arguments), only one format
  specifier (``%s``) is required.  Excess specifiers will evaluate to an
  empty string.

  When using ``${SLUG}`` with the ``-q`` option, it becomes even more
  important to use the correct number of format specifiers.  Too many
  specifiers will behave as described above; too few and ``printf`` will
  evaluate the command format string multiple times, resulting in
  unexpected output.  For example, given a queue that contains data with
  whitespace ("eenie meenie"), including only one format specifier in
  the command string:

  .. code:: console
  
     $ qrun -n -q /path/to/queue echo \"%s\"
     Dry run mode requested.
     printf will be called with unquoted arguments.
     echo "eenie"echo "meenie"
  
  The *dry run* option (``-n``) is very helpful when learning how to use
  ``${SLUG}``!



Examples
--------

Create a poor man's music playlist with a queue of file paths that are
fed by ``${SLUG}`` to your favorite media player:

.. code:: shell

   ${SLUG} /PATH/TO/QUEUE/FILE mpv

This works a lot better with the help of a few shell functions:

.. code:: bash 

   PLAYQ_FILE="${HOME}/playq.txt"

   playq() {
       # Play every path in our media queue.
       ${SLUG} "$PLAYQ_FILE" mpv \'%s\'
       [[ $? -eq 0 ]]  &&  echo "That's all, folks!"
   }
   
   playnq() {
       # Add clipboard (primary selection) contents to our media queue.
       if [[ ! -a "$PLAYQ_FILE" ]] ; then
           echo "Initializing $PLAYQ_FILE"
           dirtyq init "$PLAYQ_FILE"
       fi
       path="$(xclip -o -selection primary)"
       echo "Enqueuing $path"
       dirtyq enqueue "$PLAYQ_FILE" "$path"
   }



See Also
--------

``dirtyq`` (1)
