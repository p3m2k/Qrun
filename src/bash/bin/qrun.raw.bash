#!/usr/bin/env bash

# ${DESCRIPTION}
#
# Copyright (c) ${COPYRIGHT_YEARS} ${COPYRIGHT_OWNER} (${COPYRIGHT_CONTACT})
# SPDX-License-Identifier: ${LICENSE_SPDX_ID}



# Important Stuff
#

set -o nounset  # Disallow unset variables and parameters.
set -o noglob   # Disable pathname expansion
set -o pipefail # Pipelines return value is exit value of last failing command





# Constants
#

readonly myname="$(basename "${BASH_SOURCE[0]}")"  # $BASH_SOURCE for compatability w/ bats testing.
readonly usage_short="$myname [-h] [-v] [-V] [-n] [-f N] [-r N] [-t] [-b] [-q] QUEUE_PATH COMMAND"
readonly version="${RELEASE_VERSION}"

readonly usage_long="$usage_short
OPTIONS
    -h    Print usage
    -v    Print version
    -V    Verbose mode
    -n    Dry run mode
    -f    Limit to N consecutive failures [default 1]
    -r    Re-enqueue limit [default 0]
    -t    Do not re-enqueue data when terminated
    -b    Do not use Bash printf builtin
    -q    Do not quote printf data arguments"

readonly exit_success=0
readonly exit_error=1

readonly return_success=0
readonly return_error=1
enable printf





# Defaults
#

# Make command line options global for sake of bats testing.
declare -gi verbose=0
declare -gi dry_run=0

declare -gi re_enqueue_limit=0
declare -gi skip_re_enqueue_on_term=0
declare -gi fail_limit=1
declare -g  metadata_path="${XDG_DATA_HOME:-${HOME}/.local/share}/${SLUG}.dat"

declare -gi quote_printf_args=1





# Subroutines
#


echo_verbose() {
    # Echo output to stderr if verbose option is enabled.
    #
    # ARGS: $@ passed on unmolested
    #
    # OUTPUT: possible output to stderr
    #
    # RETURN VAL: implicit (echo)

    if [[ $verbose -eq 1 ]] ; then
        echo "$@" >&2
    fi
}


echo_func_err() {
    # Echo function error message to stderr.
    #
    # ARGS: $@ passed on unmolested
    #
    # OUTPUT: output to stderr
    #
    # RETURN VAL: implicit (echo)

    echo "${FUNCNAME[1]}: $@" >&2
}


maybe() {
    # Execute command unless dry run mode enabled; if dry run mode, echo
    # output to stdout.
    #
    # ARGS:
    local -r cmd="$@"
    #
    # OUTPUT: possible output to stderr
    #
    # RETURN VAL: implicit

    local -r trapped_signals="INT TERM QUIT"

    if [[ $dry_run -eq 1 ]] ; then
        echo "$cmd"
    else
        trap mop_up $trapped_signals
        echo_verbose "Executing command: $cmd"
        eval "$cmd" &
        local -i pid=$!
        wait $pid  # Is $pid even necessary here?
        trap - $trapped_signals
        wait $pid
    fi
}


mop_up() {
    # Handle any trapped signals.  This usually amounts to killing child
    # processes and deleting any temp files.
    #
    # ARGS: none
    #
    # OUTPUT: possible output to stderr
    #
    # RETURN VAL: none (always exits)

    echo_verbose "Mopping up..."

    local -r pids="$(jobs -p)"
    if [[ -n $pids ]] ; then
        echo_verbose "Terminating child processes: '$pids'"
        kill $pids >&2
    fi
    if [[ $skip_re_enqueue_on_term -eq 0 ]] ; then
      echo_verbose "Returning current data to queue."
      dirtyq push "$ds_path" "$d"
    fi
    exit $exit_error
}


get_checksum() {
    if [[ $# -lt 1 ]] ; then
        echo_func_err "Missing argument!"
        return $return_error
    else
        local -r data="$1"
    fi

    echo "$data" | sha256sum | cut -d ' ' -f 1
}


get_re_enqueue_count() {
    local -r data="$1"

    data_checksum="$(get_checksum "$data")"

    # Search for all data belonging to $metadata_path
    ds_metadata="$(grep "$ds_path_checksum" "$metadata_path")"
    if [[ $? -ne 0 ]] ; then
        echo 0
        return
    fi

    # Search for the current $data
    data_metadata="$(echo "$ds_metadata" | grep "$data_checksum")"
    if [[ $? -ne 0 ]]  ; then
        echo 0
        return
    fi

    local -i count="$(echo "$data_metadata" | cut -d '|' -f 3)"
    echo $count
}


set_re_enqueue_count() {
    local -r data="$1"
    local -ir new_count="$2"

    data_checksum="$(get_checksum "$data")"

    tmp_data_f="$(mktemp "${myname}.XXXXXXXXXX.dat")"
    trapped_signals="INT TERM QUIT"
    trap_action="rm -f $tmp_data_f"
    trap "$trap_action" $trapped_signals

    sed -e "/$ds_path_checksum | $data_checksum/d" "$metadata_path" >> "$tmp_data_f"
    echo "$ds_path_checksum | $data_checksum | $new_count" >> "$tmp_data_f"
    cp "$tmp_data_f" "$metadata_path"

    eval $trap_action
    trap - $trapped_signals
}


increment_re_enqueue_count() {
    local -r data="$1"

    re_enqueue_count="$(get_re_enqueue_count "$data")"
    re_enqueue_count_new=$((re_enqueue_count+1))
    set_re_enqueue_count "$data" "$re_enqueue_count_new"
}


run_command_on_data() {
    local -r data="$1"

    # Run $cmd_line on the data until it succeeds, or fails $fail_limit
    # times in a row.
    local -i fail_count=0
    until [[ $fail_count -eq $fail_limit ]] ; do
        if [[ $quote_printf_args -eq 1 ]] ; then
            maybe "$(printf "$cmd_line" "$data")"
        else
            maybe "$(printf "$cmd_line" $data)"
        fi

        if [[ $? -eq 0 ]] ; then
            [[ $dry_run -ne 1 ]]  &&  echo_verbose "Command succeeded."
            return $return_success
        else
            fail_count+=1
        fi
    done

    echo_verbose "Command failed."
    return $return_error
}


re_enqueue_data_maybe() {
    local -r data="$1"

    [[ $re_enqueue_limit -lt 1 ]]  &&  return

    local -i re_enqueue_count="$(get_re_enqueue_count "$data")"
    if [[ $re_enqueue_count -lt $re_enqueue_limit ]]; then
        echo_verbose "Re-enqueuing data."
        dirtyq enqueue "$ds_path" "$data"
        increment_re_enqueue_count "$data"
        return
    else
        echo_verbose "Re-enqueue limit reached.  Not re-enqueuing data."
        return
    fi
}


process_queue() {
    # For each data object in the queue/stack...
    while d="$(dirtyq dequeue "$ds_path")" ; do
        echo_verbose "Data: '$d'"
        run_command_on_data "$d"
        [[ $? -ne 0 ]]  &&  re_enqueue_data_maybe "$d"
    done
}


_parse_cli_opts() {
    # Process command line options.

    local -r cli_opts=":hvVnr:tf:bq"
    local opt OPTIND OPTARG OPTERR

    while getopts ":${cli_opts}" opt; do
        case $opt in
            h)
                echo "$usage_long"  # Need quotes to preserve newlines!
                exit $exit_success
                ;;
            v)
                echo "$myname $version"
                exit $exit_success
                ;;
            V)
                verbose=1
                echo "Verbose mode requested." >&2
                ;;
            n)
                dry_run=1
                echo "Dry run mode requested." >&2
                ;;
            f)
                if [[ $OPTARG -lt 1 ]] ; then
                    echo "Limit must be one or greater!"
                    echo "$usage_short"
                    exit $exit_error
                else
                    fail_limit=$OPTARG
                fi
                ;;
            r)
                if [[ $OPTARG -lt 0 ]] ; then
                    echo "Limit must be zero or greater!"
                    echo "$usage_short"
                    exit $exit_error
                else
                    re_enqueue_limit=$OPTARG
                fi
                ;;
            t)
                skip_re_enqueue_on_term=1
                ;;
            b)  
                enable -n printf
                echo "Disabling Bash built-in version of printf." >&2
                ;;  
            q)  
                quote_printf_args=0
                echo "printf will be called with unquoted arguments." >&2
                ;;  
            \?)
                echo "Invalid option: $OPTARG"
                echo "$usage_short"
                exit $exit_error
                ;;
        esac
    done

    return $((OPTIND-1))  # Number of options to discard.
}



main() {
    _parse_cli_opts "$@"  ;  shift $?

    # Check for at least 2 args.
    if [[ $# -lt 2 ]]; then
        echo "$usage_short" >&2
        return $return_error
    else
        declare -gr ds_path="$1"  &&  shift
        declare -gr ds_path_checksum="$(get_checksum "$ds_path")"
        declare -gr cmd_line="$@"
    fi

    # TODO: More better validation?
    [[ ! -e $metadata_path ]]  &&  touch $metadata_path

    process_queue
    rm -f "$metadata_path"

    # TODO: Make this something meaningful?
    return $return_success
}





if [[ "${BASH_SOURCE[0]}" == "${0}" ]] ; then
    main "$@"
    if [[ $? -ne $return_success ]] ; then
        exit $exit_error
    else
        exit $exit_success
    fi
fi
