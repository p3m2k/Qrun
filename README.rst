####
Qrun
####


Introduction
============

*Qrun* takes a path to a dirtyQ data structure and a
``printf``-formatted command line, and runs the command line on every
item in the data structure, with the data passed as the final argument.



Installation
============

Qrun is a Bash_ shell script.  Almost every Linux system includes Bash.
Users of other Unix-like_ systems may need to install it before using
this program.  Qrun also requires `dirtyq <https://gitlab.com/p3m2k/dirtyQ>`_.

There are two easy ways to install Qrun:

- Packages_ are available for users of Debian and its derivatives
  (Ubuntu, etc.).  Users of Red Hat, Fedora and friends: stay tuned!

- Download an archive of the latest release and run ``make install``:

  .. code:: console

     # wget -O - https://www.nellump.net/downloads/qrun_latest.tgz | tar xz
     # cd qrun/
     # make install

bats_ is required to run the automated test suite (of interest only to
programmers).

.. _Bash: https://www.gnu.org/software/bash/
.. _Unix-like: http://www.linfo.org/unix-like.html
.. _packages : https://www.nellump.net/computers/free_software
.. _bats: https://github.com/bats-core/bats-core



Usage
=====

See the `manpage <src/man/qrun.1.raw.rst>`_.



Future Plans
============

- Add an option to run commands in parallel.



Copying
=======

Everything here is copyright © 2022 Paul Mullen
(with obvious exceptions for any `fair use`_ of third-party content).

Everything here is offered under the terms of the MIT_ license.

    Basically, you can do whatever you want as long as you include the
    original copyright and license notice in any copy of the
    software/source.

.. _fair use: http://fairuse.stanford.edu/overview/fair-use/what-is-fair-use/
.. _MIT: https://tldrlegal.com/license/mit-license



Contact
=======

Feedback is (almost) always appreciated.  Send e-mail to
pm@nellump.net.
