# SLUG and RELEASE_VERSION are used in meta/install.txt, which can't
# abide whitespace!

# Should never change
NAME="Qrun"
SLUG="qrun"
DESCRIPTION="Runs a command on each item in a dirtyQ data structure."
AUTHOR_NAME="Paul Mullen"
AUTHOR_CONTACT="pm@nellump.net"
COPYRIGHT_OWNER="Paul Mullen"
COPYRIGHT_CONTACT="pm@nellump.net"
LICENSE_SPDX_ID="MIT"

# May need updating
COPYRIGHT_YEARS="2022"
RELEASE_VERSION="1.2.0"
RELEASE_DATE="2022-09-14"
