# Canned Sequences
#

# NB: Dollar sign sigils must be escaped unless meant for Make vars.
dolla := $



# Filters
#

define strip_comments
grep -Ev '(^$$)|(^\s*#)'
endef


# NB: Call export_substvars first!  (In same subshell!)
define substitute_vars
envsubst "$$( $(call get_substvars_shell_format) )"
endef


define get_subst_var
grep '^$(1)=' $(META_DIR)/substvars.sh | cut -d = -f 2 | tr -d \"
endef


# NB: Redirect only!  No pipe!
define export_substvars
while read ; do \
	eval export "$$REPLY" ; \
done < <(cat $(SUBST_VARS) | $(call strip_comments))
endef


define get_substvars_shell_format
for v in $$(cat $(SUBST_VARS) |  $(call strip_comments) | cut -d = -f 1 ) ; do \
	echo -n "\$$$$v " ; \
done ; \
echo
endef



# Utilities
#


define exec_make_subdir
for dir in $$(find $(1) -mindepth 1 -maxdepth 1 -type d) ; do \
	$(MAKE) -C $$dir $(2) ; \
done
endef


define echo_heading
@echo
@echo ">>>>> make $@ <<<<<"
endef


# Expects install.txt on stdin.
define install_files
(IFS=$$'\t' ; while read $(INSTALL_CFG_FIELDS) ; do \
	[[ -n $$owner ]]  &&  opts+="--owner $$owner " ; \
	[[ -n $$group ]]  &&  opts+="--group $$group " ; \
	[[ -n $$mode ]]   &&  opts+="--mode $$mode " ; \
	eval install --no-target-directory -D $$opts \"$(1)/$$source\" \"$(2)/$$dest\" ; \
done)
endef
