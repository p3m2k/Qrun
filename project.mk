# Project-Specific Configuration
#

# Meta
export NAME = $(shell $(call get_subst_var,SLUG))
export VERSION = $(shell $(call get_subst_var,RELEASE_VERSION))
export GIT_BRANCH = $(shell git branch --show-current)
export GIT_HEAD=$(shell git log -1 --pretty=format:%h)

# Build-able (and Clean-able)
# (If it's not listed here, it's invisible to the Makefile's "build" and
# "clean" targets.)
export BASH_BIN = qrun
export MANPAGES = qrun.1.gz

# Edit to taste:
export TEST_PATH_DIRS = $(BUILD_DIR)/bin:$(BUILD_DIR)/lib:$(BUILD_DIR)/libexec:$(BUILD_DIR)
